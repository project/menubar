(function ($, Drupal) {

  /**
   * Constructor for MenuBar class.
   * @param element
   *   - DOM element that triggers the menu.
   */
  function MenuBar(element) {
    this.selectors = {
      MENU_ITEM: 'li',
      MENU_LINK: 'a',
      SUB_MENU: 'ul',
    };

    this.initNavigationHelp(element);
    this.tree = this.initNavigation(element, null);
    this.initNavigationRoles(element);
    this.initRovingTabIndex(element);
    this.initNavigationKeys(element);
  };

  /**
   * Build the tree from the DOM.
   */
  MenuBar.prototype.initNavigation = function (context, parent) {
    let items = [], index = 0;

    $menus = $(context).children(this.selectors.MENU_ITEM);
    $menus.each(function (context, menuIndex, menu) {
      let $current = $(menu);
      let children = [];
      if ($current.is('.menu-item-title')) {
        return;
      }
      $link = $current.find(this.selectors.MENU_LINK).first();

      $submenu = $current.children(this.selectors.SUB_MENU);
      let item = {
        link: $link,
        submenu: null,
        parent: parent,
      };
      if ($submenu.length) {
        item.submenu = this.initNavigation($submenu.first(), item);
      }
      items.push(item);
    }.bind(this, context));

    // Siblings (wrapping).
    for (index = 0; index < items.length; index++) {
      let previous = (index - 1 >= 0) ? index - 1 : items.length - 1;
      let next = (index + 1) % items.length;
      items[index].previous = items[previous];
      items[index].next = items[next];
      items[index].last = !next;
    }
    return items;
  };

  /**
   * Attach event listeners to manage roving tab index.
   */
  MenuBar.prototype.initRovingTabIndex = function (context) {
    let index = 0;

    $('a', context).focusin(function (context, e) {
      $('a', context).attr('tabindex', '-1');
      $(e.target).attr('tabindex', '0');
    }.bind(this, context));

    $(context).focusout(function () {
      // Keep something in the tab order even if a menu was closed by blur.
      this.tree[0].link.attr('tabindex', 0);
    }.bind(this));
    // We want focus to shift on hover, because focus drives aria attributes.
    $('a', context).hover(function (context, e) {
      $(e.target).focus();
    }.bind(this, context));

    // We want to toggle expanded when menus open/close.
    for (index = 0; index < this.tree.length; index++) {
      let menu = this.tree[index];
      menu.link.parent().focusin(function (link) {
        link.attr('aria-expanded', 'true');
      }.bind(this, menu.link));

      menu.link.parent().focusout(function (link) {
        link.attr('aria-expanded', 'false');
      }.bind(this, menu.link));
    }

  };

  /**
   * Apply aria roles to one menu link.
   *
   * @param {Object} link
   */
  MenuBar.prototype.initNavigationRole = function (link) {
    let index = 0;

    // Override listitem role because the list is a menu.
    link.link.parent().attr('role', 'none');
    link.link.attr('tabindex', -1);
    link.link.attr('role', 'menuitem');
    if (link.submenu) {
      link.link.attr('aria-haspopup', 'true');
      link.link.attr('aria-expanded', 'false');
      link.link.next().attr('role', 'menu');
      link.link.next().attr('aria-label', link.link.text());

      for (index = 0; index < link.submenu.length; index++) {
        this.initNavigationRole(link.submenu[index]);
      }
    }
  };

  /**
   * Apply aria roles to entire tree.
   */
  MenuBar.prototype.initNavigationRoles = function (context) {
    let index = 0;
    $(context).attr('role', 'menubar');

    $(context).attr('aria-label', 'Navigation');

    for (index = 0; index < this.tree.length; index++) {
      this.initNavigationRole(this.tree[index]);
    }

    this.tree[0].link.attr('tabindex', 0);
  };

  /**
   * Recursively search the nav tree looking for target.
   */
  MenuBar.prototype.findNavigation = function ($target, items) {
    for (const item of items) {
      if ($target.is(item.link)) {
        return item;
      }
      if (item.submenu) {
        let sub = this.findNavigation($target, item.submenu);
        if (sub) {
          return sub;
        }
      }
    }
    return null;
  };

  /**
   * Map key press to navigation behaviors.
   *
   * <ul class="js-menubar js-menubar main">
   *   <li class="menu-item">
   *     <a href="...">Link</a>
   *     <ul class="menu">
   *        ...
   *     </ul>
   *   </li>
   *  ...
   * </ul>
   */
  MenuBar.prototype.initNavigationKeys = function (context) {
    var behavior = this;
    $(context).keydown(function (context, e) {
      var $current = $(context);
      var $target = $(e.target);
      var link = this.findNavigation($target, this.tree);
      if (!link) {
        return;
      }
      switch (e.keyCode) {
        case 37: // left
          e.preventDefault();
          this.navigateLeft(link);
          break;

        case 38: // up
          e.preventDefault();
          this.navigateUp(link);
          break;

        case 39: // right
          e.preventDefault();
          this.navigateRight(link);
          break;

        case 40: // down
          e.preventDefault();
          this.navigateDown(link);
          break;

        case 27: // escape
          e.preventDefault();
          this.navigateEscape(link);
          break;
      }
    }.bind(this, context));
  };

  /**
   * Generate navigation help elements and functionality.
   */
  MenuBar.prototype.initNavigationHelp = function (context) {
    // Create hidden dialog elements.
    var $hidden = $('<div class="hidden"></div>').appendTo('body');
    var $helpDialogEl = $('<div id="nav-help-dialog"</div>')
      .text(this.helpText()).appendTo($hidden);
    // Create help menu item. Only accessible via keyboard.
    var $help = $('<li class="menu-item menu-item--nav-help"><a href="#" role="button" tabindex="0" aria-label="Keyboard navigation help" class="menubar-nav-help visually-hidden">?</a></li>');
    $help.appendTo($(context));
    $help = $(context).find('.menubar-nav-help');
    // Show help link when focused.
    $help.focus(function ($help) {
      $help.removeClass('visually-hidden');
    }.bind(this, $help));
    // Hide help link when not focused.
    $help.blur(function ($help) {
      $help.addClass('visually-hidden');
    }.bind(this, $help));
    // Create dialog object.
    var helpDialog = Drupal.dialog($helpDialogEl, {
      title: 'Navigation help',
      modal: true,
      close: function (event, ui) {
        $help.focus()
      }
    });
    // Trigger dialog when link is clicked/keyboard enter button.
    $help.click(function () {
      if (!helpDialog.open) {
        Drupal.announce(this.helpText())
        helpDialog.showModal();
      }
      else {
        helpDialog.close();
      }
    }.bind(this));
    // Hide dialog when navigating away from help link.
    $help.keydown('keydown', function (e) {
      if (e.keyCode == 13) {
        return;
      }
      if (helpDialog.open) {
        helpDialog.close();
      }
    });
  };

  /**
   * Keyboard arrow left behavior.
   */
  MenuBar.prototype.navigateLeft = function (link) {
    if (link.parent) {
      link = link.parent;
    }
    link = link.previous;
    link.link.focus();
  };

  /**
   * Keyboard arrow right behavior.
   */
  MenuBar.prototype.navigateRight = function (link) {
    if (link.parent) {
      link = link.parent;
    }
    link = link.next;
    link.link.focus();
  };

  /**
   * Keyboard arrow up behavior.
   */
  MenuBar.prototype.navigateUp = function (link) {
    link = link.previous;
    link.link.focus();
  };

  /**
   * Keyboard arrow down behavior.
   */
  MenuBar.prototype.navigateDown = function (link) {
    if (link.submenu) {
      link = link.submenu[0];
    }
    else {
      while (link.last) {
        link = link.parent;
      }
      link = link.next;
    }
    link.link.focus();
  };

  /**
   * Keyboard escape key behavior.
   */
  MenuBar.prototype.navigateEscape = function (link) {
    if (link.parent) {
      link = link.parent;
      link.link.focus();
    }
  };

  /**
     * Navigation help text. Used for screen reader announcements and dialog.
     */
  MenuBar.prototype.helpText = function () {
    var text = 'Use the keyboard arrow keys to easily traverse the main navigation.';
    text += ' Press the escape key at any time to jump to the menu link.';
    return text;
  };

  Drupal.behaviors.menubarMenu = {
    /**
     * Attach all functionality here.
     */
    attach: function (context, settings) {

      $('.js-menubar', context).once('js-menubar-instance').each(function (index, element) {
        // Create one instance of this class per menu.
        var menuBar = new MenuBar(element);
      });
    },

  };

}(jQuery, Drupal));
