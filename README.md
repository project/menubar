CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

Menubar module adds aria roles and behaviours to a menu so that it
behaves as a menubar.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/menubar

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/menubar

REQUIREMENTS
------------

This module requires menu_link_content and menu_ui from Core.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

To use this module - you must:
* Select which menus to apply the enhancement.

MAINTAINERS
-----------
Current maintainers:
 * Damyon Wiese - https://www.drupal.org/user/3616877

This project has been sponsored by:
 * Doghouse Agency - specializing in developing drupal solutions . Visit https://doghouse.agency/ for more information
