<?php

/**
 * @file
 * Contains menubar.module.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Implements hook_help().
 */
function menubar_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the menubar module.
    case 'help.page.menubar':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Keyboard controls for menus.') . '</p>';
      $link = Link::fromTextAndUrl(
        t('WAI-ARIA Authoring Practices 1.1'),
        Url::fromUri('https://www.w3.org/TR/wai-aria-practices-1.1/#menu')
      );

      $output .= '<p>' . t('Reference: @link', ['@link' => $link->toString()]) . '</p>';
      return $output;

    default:
  }
}

/**
 * Gets the settings for menubar.
 *
 * @return \Drupal\Core\Config\ImmutableConfig
 */
function menubar_settings() {
  return \Drupal::config('menubar.settings');
}

/**
 * Implements hook_preprocess_menu().
 */
function menubar_preprocess_menu(array &$menu) {
  $settings = menubar_settings();
  $menus = $settings->get('menus');

  if ($menus && in_array($menu['menu_name'], $menus)) {
    $menu['attributes']['class'][] = Html::cleanCssIdentifier('js-menubar');
    $menu['attributes']['class'][] = Html::cleanCssIdentifier('js-menubar-top');
    $menu['#attached']['library'][] = 'menubar/menubar';
  }
}
