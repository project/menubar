<?php

namespace Drupal\menubar\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for 'menubar'.
 *
 * @package Drupal\menubar\Form
 */
class MenubarSettingsForm extends FormBase {

  /**
   * Drupals config container.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Settings key.
   *
   * @var string
   */
  protected $settingsKey = 'menubar.settings';

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return str_replace('.', '_', $this->settingsKey);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config($this->settingsKey);

    $form['menus'] = [
      '#type' => 'select',
      '#options' => menu_ui_get_menus(),
      '#multiple' => TRUE,
      '#title' => $this->t('Menus to apply the enhancement.'),
      '#description' => $this->t(
        'List of menus that should be enhanced to behave as menubars.'
      ),
      '#default_value' => $config->get('menus'),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configFactory->getEditable($this->settingsKey)
      ->set('menus', $values['menus'])
      ->save();

    $this->messenger()->addMessage('Settings have been saved');
  }

}
